'use strict'

/*
Теорія:

1. Опишіть своїми словами, що таке метод об'єкту
Метод об'єкту - це функція вбудована в об'єкт, призначена для виконання певних дій з його властивостями або з
властивостями пов'язаних сутностей. Метод об'єкту доступний для виклику до тих пір, поки об'єкт існує у пам'яті.
2. Який тип даних може мати значення властивості об'єкта?
Будь-який тип даних javascript.
3. Об'єкт це посилальний тип даних. Що означає це поняття?
Це ознанчає, що при присвоюванні об'єкта в якості значення змінної або властивості іншого об'єкту
дані зберігатимуться у вихідному об'єкті, відповідно, дії також відбуватимуться над вихідним об'єктом.
Наприклад:

const user1 = {name: 'Petro', lastName: 'Sagaidachnyi'};
const user2 = user1;
user2.name = 'Ivan';
console.log(user1); //name = 'Ivan'

Задача:
Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:
Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
При виклику функція повинна запитати ім'я та прізвище.
Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.
Необов'язкове завдання підвищеної складності
Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
*/


function createNewUser () {
    const newUser = {
        _firstName: '',
        _lastName: '',
        getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },
        setFirstName (value) {
            Object.defineProperty(this, '_firstName', {
                writable: true,
            });
            this._firstName = value;
            Object.defineProperty(this, '_firstName', {
                writable: false,
            });
        },
        setLastName (value) {
            Object.defineProperty(this, '_lastName', {
                writable: true,
            });
            this._lastName = value;
            Object.defineProperty(this, '_lastName', {
                writable: false,
            });
        },

        getFirstName () {
            return this._firstName;
        },
        getLastName () {
            return this._lastName;
        },
    };

    Object.defineProperties(newUser, {
        '_firstName': { writable: false},
        '_lastName': { writable: false},
    });

    newUser.setFirstName(prompt('Your name?'));
    newUser.setLastName(prompt ('Your last name?'));
    return newUser;
}

let user1 = createNewUser();

/*user1._firstName = 'user1-firstname';*/

console.log(user1.getLogin());
console.log(user1);